import "./App.css";
import React from "react";

import LandingPage from "./components/landingpage/landingpage";
import { Route, Switch } from "react-router-dom";
import { BrowserRouter as Router } from "react-router-dom";
import Signup from "./components/authorization/signup";
import Login from "./components/authorization/login";
import Catalog from "./components/catalog/catalog";
import Navigation from "./components/navigation/navigation";
import Profile from "./components/profile/profile";
import Cart from "./components/cart/cart";
import CustomSnackbar from "./components/common/snackbar";
import ComingSoon from "./components/comingsoon";

function App() {
  return (
    <div className="App">
      <Router>
        <CustomSnackbar />
        <Switch>
          <Route exact path="/">
            <LandingPage />
          </Route>
          <Route path="/register">
            <Signup />
          </Route>
          <Route path="/login">
            <Login />
          </Route>
          <Route>
            <Navigation />
            <Route path="/catalog/:productId?">
              <Catalog />
            </Route>

            <Route path="/profile">
              <Profile />
            </Route>
            <Route path="/cart">
              <Cart />
            </Route>
            <Route path="/inventory">
              <ComingSoon />
            </Route>
          </Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
