import { Drawer, ListItem, ListItemText, List } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, withRouter } from "react-router";
import { DrawerStateAction } from "../reducers/ProductAction";
import "./drawer.css";
const LandingDrawer = (list) => {
  const history = useHistory();
  const drawerState = useSelector((state) => state.drawerState);

  const useStyles = makeStyles({
    drawerPaper: {
      marginTop: "60px",
      color: "#FFF",
      backgroundColor: "rgba(36, 41, 62, 0.8)",
    },
  });

  const classes = useStyles();
  const dispatch = useDispatch();

  const itemList = [
    { text: "ABOUT" },
    { text: "CATALOG" },
    { text: "PRICING" },
    { text: "SUPPLIERS" },
    { text: "HELP CENTER" },
    { text: "BLOG" },
    { text: "SIGN UP NOW", onClick: () => history.push("/register") },
    { text: "LOGIN", onClick: () => history.push("/login") },
  ];

  return (
    <Drawer
      open={drawerState}
      variant="temporary"
      anchor="top"
      classes={{
        paper: classes.drawerPaper,
      }}
      onClose={() => dispatch(DrawerStateAction(false))}
    >
      <List>
        {itemList.map((item, index) => {
          const { text, onClick } = item;
          return (
            <ListItem button key={text} onClick={onClick}>
              <ListItemText primary={text} />
            </ListItem>
          );
        })}
      </List>
    </Drawer>
  );
};

export default withRouter(LandingDrawer);
