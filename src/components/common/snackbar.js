import Snackbar from "@material-ui/core/Snackbar";
import Alert from "@material-ui/lab/Alert";
import { useDispatch, useSelector } from "react-redux";
import { snackbarStateAction } from "../reducers/ProductAction";

const CustomSnackbar = () => {
  const dispatch = useDispatch();
  const snackbarMessage = useSelector((state) => state.snackbarStyle);
  // console.log(snackbarMessage.message);
  const closeSnackbar = () => {
    dispatch(snackbarStateAction({ ...snackbarMessage, state: false }));
  };
  console.log("skintobone", snackbarMessage.state);
  return (
    <Snackbar
      anchorOrigin={{
        vertical: "bottom",
        horizontal: "right",
      }}
      open={snackbarMessage.state}
      onClose={closeSnackbar}
      autoHideDuration={3000}
    >
      <Alert severity={snackbarMessage.severity} onClose={closeSnackbar}>
        {snackbarMessage.message}
      </Alert>
    </Snackbar>
  );
};

export default CustomSnackbar;
