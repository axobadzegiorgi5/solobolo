import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Slider from "@material-ui/core/Slider";

const useStyles = makeStyles({
  root: {
    width: "90%",
    trackColor: "yellow",
    selectionColor: "green",
  },
  slider: {
    trackColor: "yellow",
    selectionColor: "red",
  },
});

function valuetext(value) {
  return `${value}`;
}

const SliderBar = (title) => {
  const classes = useStyles();
  const [value, setValue] = React.useState([1, 100]);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  return (
    <div className={classes.root}>
      <Slider
        value={value}
        onChange={handleChange}
        aria-labelledby="range-slider"
        getAriaValueText={valuetext}
      />
      <div className="slider-value-container">
        <div className="slider-value-wrapper">
          <div className="slider-value">
            <div className="slider-currency">
              <span>$</span>
            </div>
            <div className="slider-amount">
              <span>{value[0]}</span>
            </div>
          </div>
        </div>
        <div className="slider-value">
          <div className="slider-currency">
            <span>$</span>
          </div>
          <div className="slider-amount">
            <span>{value[1]}</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SliderBar;
