import { Drawer, ListItem, ListItemText, List } from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import { useDispatch, useSelector } from "react-redux";
import { useHistory, withRouter } from "react-router";
import { DrawerStateAction } from "../reducers/ProductAction";
import "./drawer.css";
const CatalogDrawer = (list) => {
  const history = useHistory();
  const drawerState = useSelector((state) => state.drawerState);

  const useStyles = makeStyles({
    drawerPaper: {
      borderLeft: "1px solid #dde3ee;",
      color: "#677791",
    },
  });

  const classes = useStyles();
  const dispatch = useDispatch();

  const itemList = [
    { text: "Profile", onClick: () => history.push("/profile") },
    { text: "Dashboard", onClick: () => history.push("/dashboard") },
    { text: "Catalog", onClick: () => history.push("/catalog") },
    { text: "Inventory", onClick: () => history.push("/inventory") },
    { text: "Cart", onClick: () => history.push("/cart") },
    { text: "Orders", onClick: () => history.push("/orders") },
    { text: "Transactions", onClick: () => history.push("/transactions") },
    { text: "Stores List", onClick: () => history.push("/storeslist") },
  ];

  return (
    <Drawer
      open={drawerState}
      variant="temporary"
      anchor="right"
      classes={{
        paper: classes.drawerPaper,
      }}
      onClose={() => dispatch(DrawerStateAction(false))}
    >
      <List>
        {itemList.map((item, index) => {
          const { text, onClick } = item;
          return (
            <ListItem button key={text} onClick={onClick}>
              <ListItemText primary={text} />
            </ListItem>
          );
        })}
      </List>
    </Drawer>
  );
};

export default withRouter(CatalogDrawer);
