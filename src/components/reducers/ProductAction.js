import {
  PRODUCTS_FETCHED,
  PRODUCTS_SORTED,
  MODAL_OPENED,
  CART_FETCHED,
  PRODUCTS_SEARCHED,
  SNACKBAR_OPENED,
  EDITOR_OPENED,
  PRODUCT_MODAL_OPENED,
  DRAWER_OPENED,
  FILTER_OPENED,
  PRODUCT_CHECKED,
  PRODUCT_UNCHECKED,
  CHECK_ALL,
} from "./ActionTypes";

export const getProductsAction = (products) => {
  return {
    type: PRODUCTS_FETCHED,
    payload: products,
  };
};

export const sortProductsActions = (products) => {
  return {
    type: PRODUCTS_SORTED,
    payload: products,
  };
};

export const openModalAction = (state) => {
  return {
    type: MODAL_OPENED,
    payload: state,
  };
};

export const getCartAction = (products) => {
  return {
    type: CART_FETCHED,
    payload: products,
  };
};

export const searchProductsAction = (products) => {
  return {
    type: PRODUCTS_SEARCHED,
    payload: products,
  };
};

export const snackbarStateAction = (state) => {
  console.log(state);
  return {
    type: SNACKBAR_OPENED,
    payload: state,
  };
};

export const editorStateAction = (state) => {
  return {
    type: EDITOR_OPENED,
    payload: state,
  };
};

export const ProductModalStateAction = (state) => {
  return {
    type: PRODUCT_MODAL_OPENED,
    payload: state,
  };
};

export const DrawerStateAction = (state) => {
  return {
    type: DRAWER_OPENED,
    payload: state,
  };
};

export const FilterStateAction = (state) => {
  return {
    type: FILTER_OPENED,
    payload: state,
  };
};

export const CheckProductAction = (product) => {
  return {
    type: PRODUCT_CHECKED,
    payload: product,
  };
};

export const UncheckProductAction = (product) => {
  return {
    type: PRODUCT_UNCHECKED,
    payload: product,
  };
};

export const SelectAllAction = (product) => {
  return {
    type: CHECK_ALL,
    payload: product,
  };
};
