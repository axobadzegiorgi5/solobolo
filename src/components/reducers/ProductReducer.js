import {
  PRODUCTS_FETCHED,
  PRODUCTS_SORTED,
  MODAL_OPENED,
  CART_FETCHED,
  PRODUCTS_SEARCHED,
  SNACKBAR_OPENED,
  EDITOR_OPENED,
  PRODUCT_MODAL_OPENED,
  DRAWER_OPENED,
  FILTER_OPENED,
  PRODUCT_CHECKED,
  PRODUCT_UNCHECKED,
  CHECK_ALL,
} from "./ActionTypes";

const initState = {
  productList: [],
  displayedProductList: [],
  cartProducts: [],
  checkedProductList: [],
  searchValue: "",
  modalState: false,
  editorState: { state: false, id: "" },
  snackbarStyle: { state: false, message: "", severity: "" },
  productModalState: false,
  drawerState: false,
  filterState: false,
};

const ProductsReducer = (state = initState, action) => {
  switch (action.type) {
    case PRODUCTS_FETCHED:
      return {
        ...state,
        productList: action.payload,
        displayedProductList: action.payload,
      };
    case PRODUCTS_SORTED:
      return {
        ...state,
        displayedProductList: action.payload,
        searchValue: action.payload,
      };
    case MODAL_OPENED:
      return {
        ...state,
        modalState: action.payload,
      };
    case CART_FETCHED:
      return {
        ...state,
        cartProducts: action.payload,
      };
    case PRODUCTS_SEARCHED:
      return {
        ...state,
        displayedProductList: action.payload,
      };
    case SNACKBAR_OPENED:
      return {
        ...state,
        snackbarStyle: action.payload,
      };
    case EDITOR_OPENED:
      return {
        ...state,
        editorState: action.payload,
      };
    case PRODUCT_MODAL_OPENED:
      return {
        ...state,
        productModalState: action.payload,
      };
    case DRAWER_OPENED:
      return {
        ...state,
        drawerState: action.payload,
      };
    case FILTER_OPENED:
      return {
        ...state,
        filterState: action.payload,
      };
    case PRODUCT_CHECKED:
      return {
        ...state,
        checkedProductList: [...state.checkedProductList, action.payload],
      };
    case PRODUCT_UNCHECKED:
      return {
        ...state,
        checkedProductList: [
          ...state.checkedProductList.filter((item) => item !== action.payload),
        ],
      };
    case CHECK_ALL:
      return {
        ...state,
        checkedProductList: action.payload,
      };
    default:
      return { ...state };
  }
};

export default ProductsReducer;
