import "./comingsoon.css";
import { ReactComponent as ComingSoonSVG } from "./comingsoon.svg";
const ComingSoon = () => {
  return (
    <div className="coming-soon">
      <ComingSoonSVG />
      <p>Page is under construction.</p>
    </div>
  );
};

export default ComingSoon;
