export const sortItems = (sortValue, products) => {
  if (sortValue === "asc") {
    products.sort((a, b) => a.price - b.price);
  } else if (sortValue === "desc") {
    products.sort((a, b) => b.price - a.price);
  } else if (sortValue === "InAlphabet") {
    products.sort((a, b) => a.title.localeCompare(b.title));
  } else if (sortValue === "Alphabet") {
    products.sort((a, b) => b.title.localeCompare(a.title));
  } else {
    products.sort((a, b) => a.id - b.id);
  }

  return products;
};
