import SliderBar from "../common/slider";
import Dropdown from "react-dropdown";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { useDispatch, useSelector } from "react-redux";
import { FilterStateAction } from "../reducers/ProductAction";
const CatalogNavbar = () => {
  const NavbarOpen = useSelector((state) => state.filterState);

  const dispatch = useDispatch();
  return (
    <div className={NavbarOpen ? "catalog__navbar--active" : "catalog__navbar"}>
      <div className="catalog__navbar-selector navbar-niche--purple">
        <span>Choose Niche</span>
        <ExpandMoreIcon />
      </div>
      <div className="catalog__navbar-selector navbar-niche--pink navbar-shadow">
        <span>Choose Category</span>
        <ExpandMoreIcon />
      </div>
      <button
        className="navbar-back"
        onClick={() => dispatch(FilterStateAction(false))}
      >
        <ArrowBackIcon />
        Back
      </button>
      <div className="dropdowns__container">
        <Dropdown placeholder="Ship From" />
        <Dropdown placeholder="Ship To" />
        <Dropdown placeholder="Select Supplier" />
      </div>

      <div className="sliders__container">
        <div className="slider-wrapper">
          <div className="slider-title">
            <span>PRICE RANGE</span>
          </div>
          <SliderBar />
        </div>
        <div className="slider-wrapper">
          <div className="slider-title">
            <span>PROFIT RANGE</span>
          </div>
          <SliderBar />
        </div>
      </div>
    </div>
  );
};

export default CatalogNavbar;
