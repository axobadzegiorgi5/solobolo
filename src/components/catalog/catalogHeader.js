import CheckCircleIcon from "@material-ui/icons/CheckCircle";
import MenuIcon from "@material-ui/icons/Menu";
import searchButtonPNG from "./catalogImages/search-button.PNG";
import searchButtonGrayPNG from "./catalogImages/search-button-gray.PNG";
import filterIconPNG from "./catalogImages/filter-icon.svg";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { sortItems } from "./productFunctions";
import { DrawerStateAction, SelectAllAction } from "../reducers/ProductAction";
import {
  FilterStateAction,
  searchProductsAction,
} from "../reducers/ProductAction";
import CloseButtonPNG from "./catalogImages/close-button.PNG";
const CatalogHeader = ({ productList, sortValue }) => {
  const OriginalproductList = useSelector((state) => state.productList);
  const dispatch = useDispatch();
  const displayedProductList = useSelector((state) => state.productList);
  const checkedProductList = useSelector((state) => state.checkedProductList);

  const [searchValue, setSearchValue] = useState("");
  const [searchExpand, setSearchExpand] = useState(false);
  const search = (searchQuery) => {
    if (searchQuery) {
      const newList = displayedProductList.filter((item) =>
        item.title.toLowerCase().includes(searchQuery)
      );
      dispatch(searchProductsAction(newList));
    } else if (searchQuery === "") {
      const sortChecked = sortItems(sortValue, displayedProductList);
      dispatch(searchProductsAction(sortChecked));
    }
  };

  const selectAll = () => {
    const checkedList = OriginalproductList.map((item) => {
      return (item = item.id);
    });
    dispatch(SelectAllAction(checkedList));
  };

  const clearAll = () => {
    dispatch(SelectAllAction([]));
  };

  return (
    <header className="catalog__header">
      <div className="catalog__header-left">
        <button className="header-btn select-all" onClick={() => selectAll()}>
          SELECT ALL
        </button>
        <div className="seperate"></div>
        <button
          className="header-btn filter-btn-small"
          onClick={() => dispatch(FilterStateAction(true))}
        >
          FILTER{" "}
        </button>
        <button
          className="header-btn filter-btn-icon"
          onClick={() => dispatch(FilterStateAction(true))}
        >
          <img src={filterIconPNG} alt="filter" />
        </button>
        <span className="product-selected-amount">
          <span>Selected {checkedProductList.length} out of </span>
          {productList.length} products
        </span>
        <button
          className={`header-btn clear-selected ${
            checkedProductList.length > 0 ? " clear-selected--active" : ""
          }`}
          onClick={() => clearAll()}
        >
          CLEAR
        </button>
        <button
          className="header-btn select-all-small"
          onClick={() => selectAll()}
        >
          <CheckCircleIcon />
        </button>
      </div>
      <div className="catalog__header-right">
        <div
          className={`header__search-container ${
            searchExpand ? " header__search-container--active" : ""
          }`}
        >
          <button
            className={`search-small ${
              searchExpand ? " search-small--active" : ""
            }`}
            onClick={() => setSearchExpand(true)}
          >
            <img src={searchButtonGrayPNG} alt="search" />
          </button>
          <input
            onKeyPress={(event) => {
              if (event.key === "Enter") {
                search(searchValue);
              }
            }}
            type="text"
            className={`header__search ${
              searchExpand ? " header__search--active" : ""
            }`}
            placeholder="Search..."
            onChange={(e) => setSearchValue(e.target.value)}
          ></input>
          <div className={"search-btn"} onClick={() => search(searchValue)}>
            <img src={searchButtonPNG} alt="search" />
          </div>
          <div
            className={`clear-btn ${searchExpand ? "clear-btn--active" : ""}`}
            onClick={() => setSearchExpand(false)}
          >
            <img src={CloseButtonPNG} alt="search" />
          </div>
        </div>
        <button
          className={`header-btn add-to-inventory ${
            searchExpand ? "add-to-inventory--hidden" : ""
          }`}
        >
          ADD TO INVENTORY
        </button>
        <button
          className={`header-btn add-to-inventory-small ${
            searchExpand ? "add-to-inventory--hidden" : ""
          }`}
        >
          ADD
        </button>
        <div
          className="header__burger"
          onClick={() => dispatch(DrawerStateAction(true))}
        >
          <MenuIcon />
        </div>

        <button className="help-menu">
          <span>?</span>
        </button>
      </div>
    </header>
  );
};

export default CatalogHeader;
