import "./product.css";
import DeleteIcon from "@material-ui/icons/Delete";
import EditIcon from "@material-ui/icons/Edit";
import { addToCartAPI, deleteProductAPI } from "../API";
import { getProducts } from "../API";
import { useDispatch, useSelector } from "react-redux";
import {
  getProductsAction,
  snackbarStateAction,
  editorStateAction,
  ProductModalStateAction,
  UncheckProductAction,
  CheckProductAction,
} from "../reducers/ProductAction";
import { useHistory } from "react-router";

const Product = ({ price, title, image, id }) => {
  const checkedProductList = useSelector((state) => state.checkedProductList);

  const dispatch = useDispatch();
  const history = useHistory();
  const deleteProduct = async (id) => {
    try {
      await deleteProductAPI(id);
      await dispatch(
        snackbarStateAction({
          state: true,
          message: "Product Deleted Successfully",
          severity: "success",
        })
      );
      await getProducts().then((result) => {
        dispatch(getProductsAction(result.data.data));
      });
    } catch (err) {
      console.log(err);
      dispatch(
        snackbarStateAction({
          state: true,
          message: `${err}`,
          severity: "error",
        })
      );
    }
  };

  const addProduct = async (id, qty) => {
    try {
      await addToCartAPI(id, qty);
      await dispatch(
        snackbarStateAction({
          state: true,
          message: "Product Added To Cart Successfully",
          severity: "success",
        })
      );
    } catch (err) {
      await dispatch(
        snackbarStateAction({
          state: true,
          message: `${err}`,
          severity: "error",
        })
      );
    }
  };

  const openEditor = () => {
    dispatch(editorStateAction({ state: true, id: id }));
    console.log(id);
  };

  const openProductModal = (id) => {
    history.push(`/catalog/${id}`);
    dispatch(ProductModalStateAction(true));
  };

  const selectProduct = (id) => {
    const checked = checkedProductList.findIndex((item) => item === id);
    if (checked > -1) {
      dispatch(UncheckProductAction(id));
    } else {
      dispatch(CheckProductAction(id));
    }
  };

  return (
    <div
      className={`product__container ${
        checkedProductList.includes(id) ? "product__container--selected" : ""
      }`}
    >
      <div
        className={`product__header ${
          checkedProductList.includes(id) ? "product__header--visible" : ""
        }`}
      >
        <input
          type="checkbox"
          className="product-checkbox"
          checked={checkedProductList.includes(id)}
          onClick={() => selectProduct(id)}
        ></input>
        <div
          className={`product__header-buttons ${
            checkedProductList.includes(id)
              ? "product__header-buttons--hidden"
              : ""
          }`}
        >
          <button className="product-delete" onClick={() => deleteProduct(id)}>
            <DeleteIcon />
          </button>
          <button className="product-edit" onClick={() => openEditor()}>
            <EditIcon />
          </button>
          <button className="product-add" onClick={() => addProduct(id, 1)}>
            ADD TO INVENTORY
          </button>
        </div>
      </div>
      <div className="product-below" onClick={() => openProductModal(id)}>
        <div className="product-image">
          <img src={image} alt="product"></img>
        </div>
        <div className="product__title">
          <span>{title}</span>
        </div>
        <div className="product-supplier">
          <span>By: US-Supplier116</span>
        </div>
        <div className="product__prices">
          <ul className="price-list">
            <li className="price-box">
              <strong>130</strong>
              <span>RRP</span>
            </li>
            <li className="price-box">
              <strong>${price}</strong>
              <span>COST</span>
            </li>
            <li className="price-box">
              <strong className="text-cyan">35% ($7)</strong>
              <span>PROFIT</span>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Product;
