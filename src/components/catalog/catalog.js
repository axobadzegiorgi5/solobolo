import "./catalog.css";
import Product from "./product";
import "react-dropdown/style.css";
import { getProducts } from "../API";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  getProductsAction,
  openModalAction,
  ProductModalStateAction,
  sortProductsActions,
} from "../reducers/ProductAction";
import { sortItems } from "./productFunctions";
import CatalogNavbar from "./catalogNavbar";
import CatalogHeader from "./catalogHeader";
import sortIcon from "./catalogImages/sort-icon.PNG";

import AddModal from "./addmodal";
import AddIcon from "@material-ui/icons/Add";

import EditModal from "./editmodal";
import ProductModal from "./productmodal";
import { useParams } from "react-router";
import CatalogDrawer from "../common/catalogDrawer";
const Catalog = () => {
  const dispatch = useDispatch();
  const { productId } = useParams();

  useEffect(() => {
    getProducts().then((res) => dispatch(getProductsAction(res.data.data)));
    if (productId !== undefined) {
      dispatch(ProductModalStateAction(true));
    }
  }, []);

  const displayedProductList = useSelector(
    (state) => state.displayedProductList
  );
  const [sortvalue, setSortValue] = useState("");

  useEffect(() => {
    const sorted = sortItems(sortvalue, displayedProductList);
    dispatch(sortProductsActions([...sorted]));
  }, [sortvalue]);

  const openModal = () => {
    dispatch(openModalAction(true));
  };

  return (
    <div className="catalog">
      <CatalogNavbar />
      <AddModal />
      <EditModal />
      <ProductModal />
      <CatalogDrawer />
      <div className="catalog__wrapper">
        <CatalogHeader
          productList={displayedProductList}
          sortValue={sortvalue}
        />
        <div className="catalog__sort">
          <div className="catalog__sort-input">
            <img src={sortIcon} alt="sort" />
            <span>Sort By:</span>
            <select
              onChange={(e) => setSortValue(e.target.value)}
              className="catalog__sort-select"
              placeholder="sort"
            >
              <option value="id">New Arrivals</option>
              <option value="asc">PRICE: Low To High</option>
              <option value="desc">PRICE: High To Low</option>
              <option value="InAlphabet">ALPHABET: A-Z</option>
              <option value="Alphabet">ALPHABET: Z-A</option>
            </select>
          </div>
          <button
            className="header-btn add-product-btn--small"
            onClick={openModal}
          >
            <AddIcon />
          </button>
          <button className="header-btn add-product-btn" onClick={openModal}>
            Add Product
          </button>
        </div>
        <div className="catalog__products">
          {displayedProductList.map((product) => (
            <Product
              key={product.id}
              price={product.price}
              title={product.title}
              image={product.imageUrl}
              id={product.id}
            />
          ))}
        </div>
      </div>
    </div>
  );
};

export default Catalog;
