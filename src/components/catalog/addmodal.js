import "./addmodal.css";
import TextField from "@material-ui/core/TextField";
import { useFormik } from "formik";
import * as Yup from "yup";
import { addProductAPI, getProducts } from "../API";
import { makeStyles } from "@material-ui/core/styles";
import ClearIcon from "@material-ui/icons/Clear";
import { useDispatch, useSelector } from "react-redux";
import { getProductsAction, openModalAction } from "../reducers/ProductAction";
import { snackbarStateAction } from "../reducers/ProductAction";
const AddProductValidation = Yup.object({
  title: Yup.string().required("Title Required"),
  description: Yup.string().required("Description Required"),
  price: Yup.number()
    .typeError("Field must be a number")
    .required("Price required"),
  imageUrl: Yup.string().required("Image required"),
});

const AddModal = () => {
  const useStyles = makeStyles({
    field: {
      height: "78px",
    },
  });
  const classes = useStyles();
  const dispatch = useDispatch();
  const modalState = useSelector((state) => state.modalState);

  const productAddForm = useFormik({
    initialValues: { title: "", description: "", price: "", imageUrl: "" },
    onSubmit: async (values, { resetForm }) => {
      try {
        await addProductAPI(
          values.title,
          values.description,
          values.price,
          values.imageUrl
        );
        await dispatch(openModalAction(false));
        await getProducts().then((res) =>
          dispatch(getProductsAction(res.data.data))
        );
        await dispatch(
          snackbarStateAction({
            state: true,
            message: "Product Added Successfully",
            severity: "success",
          })
        );
        await resetForm();
      } catch (err) {
        dispatch(
          snackbarStateAction({
            state: true,
            message: `${err}`,
            severity: "error",
          })
        );
      }
    },
    validationSchema: AddProductValidation,
  });

  const closeModal = () => {
    dispatch(openModalAction(false));
  };

  return (
    <div
      className={`modal__container ${
        modalState ? " modal__container--show" : ""
      }`}
    >
      <div className="modal__background" onClick={closeModal} />
      <form className="modal__card" onSubmit={productAddForm.handleSubmit}>
        <div className="modal__header">
          <div className="modal__title">
            <span className="modal-title">Create a new Product</span>
          </div>
          <div className="modal__exit" onClick={closeModal}>
            <ClearIcon />
          </div>
        </div>
        <TextField
          className={classes.field}
          variant="outlined"
          placeholder="Title"
          error={
            productAddForm.touched.title && Boolean(productAddForm.errors.title)
          }
          helperText={
            productAddForm.touched.title && productAddForm.errors.title
          }
          value={productAddForm.values.title}
          onChange={productAddForm.handleChange}
          name="title"
          id="title"
        ></TextField>
        <TextField
          className={classes.field}
          variant="outlined"
          placeholder="Price"
          error={
            productAddForm.touched.price && Boolean(productAddForm.errors.price)
          }
          value={productAddForm.values.price}
          helperText={
            productAddForm.touched.price && productAddForm.errors.price
          }
          onChange={productAddForm.handleChange}
          name="price"
          id="price"
        ></TextField>
        <TextField
          className={classes.field}
          variant="outlined"
          placeholder="image URL"
          error={
            productAddForm.touched.imageUrl &&
            Boolean(productAddForm.errors.imageUrl)
          }
          value={productAddForm.values.imageUrl}
          helperText={
            productAddForm.touched.imageUrl && productAddForm.errors.imageUrl
          }
          onChange={productAddForm.handleChange}
          name="imageUrl"
          id="imageUrl"
        ></TextField>
        <TextField
          className={classes.field}
          variant="outlined"
          placeholder="Description"
          error={
            productAddForm.touched.description &&
            Boolean(productAddForm.errors.description)
          }
          value={productAddForm.values.description}
          helperText={
            productAddForm.touched.description &&
            productAddForm.errors.description
          }
          onChange={productAddForm.handleChange}
          name="description"
          id="description"
        ></TextField>
        <div className="modal__add--btn">
          <button className="header-btn add-btn" type="submit">
            Add Product
          </button>
        </div>
      </form>
    </div>
  );
};

export default AddModal;
