import "./productmodal.css";
import ClearIcon from "@material-ui/icons/Clear";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router";
import {
  ProductModalStateAction,
  snackbarStateAction,
} from "../reducers/ProductAction";
import { addToCartAPI, singleProductAPI } from "../API";
const ProductModal = () => {
  const history = useHistory();
  const dispatch = useDispatch();
  const [Product, OpenProduct] = useState({});
  const productModalState = useSelector((state) => state.productModalState);
  const { productId } = useParams();

  const addToCart = async (id, qty) => {
    try {
      await addToCartAPI(id, qty);
      await dispatch(
        snackbarStateAction({
          state: true,
          message: "Product Added To Cart successfully",
          severity: "success",
        })
      );
    } catch (err) {
      console.log(err);
      await dispatch(
        snackbarStateAction({
          state: true,
          message: `${err}`,
          severity: "error",
        })
      );
    }
  };
  useEffect(() => {
    if (productId !== undefined) {
      singleProductAPI(productId).then((res) => {
        OpenProduct(res.data.data);
      });
    }
  }, [productId]);

  const closeProductModal = () => {
    OpenProduct({});
    history.push("/catalog");
    dispatch(ProductModalStateAction(false));
  };
  console.log(Product);
  if (productModalState === true) {
    return (
      <div className="productmodal">
        <div className="productmodal__card">
          <div className="productmodal__card-left">
            <div className="card__left-pricelist">
              <div className="price-container">
                <span className="price">${Product.price}</span>
                <span className="price-title">RRP</span>
              </div>
              <div className="price-container">
                <span className="price">${Product.price}</span>
                <span className="price-title">COST</span>
              </div>
              <div className="price-container">
                <span className="price price--cyan">24% ($11)</span>
                <span className="price-title">PROFIT</span>
              </div>
            </div>
            <div className="card-slideshow">
              <div className="slideshow-image">
                <img src={Product.imageUrl} alt="slideshow-img"></img>
              </div>
              <div className="slideshow-pagination">
                <ul className="pagination-ul">
                  <li className="pagination-img">
                    <img src={Product.imageUrl} alt="pagination-img" />
                  </li>
                  <li className="pagination-img">
                    <img src={Product.imageUrl} alt="pagination-img" />
                  </li>
                  <li className="pagination-img">
                    <img src={Product.imageUrl} alt="pagination-img" />
                  </li>
                  <li className="pagination-img">
                    <img src={Product.imageUrl} alt="pagination-img" />
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="productmodal__card-right">
            <div className="productmodal-header">
              <div className="productmodal-supplier">SKU# puk-xepp47</div>

              <span>Supplier:SP-Supplier118</span>
            </div>
            <h2 className="productmodal-title">{Product.title}</h2>
            <div className="productmodal-buttons">
              <button onClick={() => addToCart(Product.id, 1)}>
                ADD TO INVENTORY
              </button>
            </div>
            <div className="productmodal-tabs">
              <div className="tab tab--active">Product Details</div>
              <div className="tab">Shipping Rates</div>
            </div>
            <div className="productmodal-description">
              <span>{Product.description}</span>
            </div>
            <div
              className="productmodal-close"
              onClick={() => closeProductModal()}
            >
              <ClearIcon />
            </div>
          </div>
        </div>
        <div
          className="productmodal__background"
          onClick={() => closeProductModal()}
        ></div>
      </div>
    );
  }
  return null;
};

export default ProductModal;
