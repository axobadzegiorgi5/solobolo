import "./editmodal.css";
import ClearIcon from "@material-ui/icons/Clear";
import * as Yup from "yup";
import { useFormik } from "formik";
import { TextField, makeStyles } from "@material-ui/core";
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  editorStateAction,
  getProductsAction,
  snackbarStateAction,
} from "../reducers/ProductAction";
import { editProductAPI, getProducts, singleProductAPI } from "../API";

const EditProductValidation = Yup.object({
  title: Yup.string().required().min(5).max(50),
  description: Yup.string().required().min(0).max(300),
  price: Yup.string().required().min(0),
  imageUrl: Yup.string().required().url(),
});

const useStyles = makeStyles({
  descriptionInputField: {},
  textForm: {
    backgroundColor: "#5309ff04",
    borderTopLeftRadius: "2px",
    borderTopRightRadius: "2px",
  },
});

const EditModal = () => {
  const classes = useStyles();
  const [product, setProduct] = useState({});
  const editorState = useSelector((state) => state.editorState);
  const dispatch = useDispatch();

  useEffect(() => {
    editorState.id &&
      singleProductAPI(editorState.id).then((res) => {
        console.log("This is Result:", res.data.data);
        setProduct(res.data.data);
      });
  }, [editorState.id]);

  const productEditForm = useFormik({
    initialValues: {
      title: product.title,
      description: product.description,
      price: product.price,
      imageUrl: product.imageUrl,
    },

    onSubmit: async (values, { resetForm }) => {
      try {
        await editProductAPI(
          product.id,
          values.title,
          values.description,
          values.price,
          values.imageUrl
        );
        await getProducts().then((res) =>
          dispatch(getProductsAction(res.data.data))
        );

        await closeEditor();
        await resetForm();
        await dispatch(
          snackbarStateAction({
            state: "true",
            message: "Product Edited Successfully",
            severity: "success",
          })
        );
      } catch (err) {
        dispatch(
          snackbarStateAction({
            state: "true",
            message: `${err}`,
            severity: "error",
          })
        );
      }
    },
    validationSchema: EditProductValidation,
    enableReinitialize: true,
  });

  const closeEditor = () => {
    dispatch(editorStateAction({ state: "false", id: "" }));
  };

  if (editorState.state === true) {
    return (
      product && (
        <div className="editmodal">
          <div className="editmodal__container">
            <div className="editmodal__header">
              <span className="editmodal__title">Edit Product</span>
              <div className="editmodal__exit" onClick={() => closeEditor()}>
                <ClearIcon />
              </div>
            </div>
            <form
              className="editmodal__form"
              onSubmit={productEditForm.handleSubmit}
            >
              <div className="editmodal__textfield">
                <p>Product Title:</p>
                <TextField
                  fullWidth
                  className={classes.textForm}
                  value={productEditForm.values.title}
                  onChange={productEditForm.handleChange}
                  error={
                    productEditForm.touched.title &&
                    Boolean(productEditForm.errors.title)
                  }
                  id="title"
                  name="title"
                  placeholder="Title"
                ></TextField>
              </div>
              <div className="editmodal__textfield">
                <p>Product Price:</p>
                <TextField
                  fullWidth
                  className={classes.textForm}
                  value={productEditForm.values.price}
                  onChange={productEditForm.handleChange}
                  error={
                    productEditForm.touched.price &&
                    Boolean(productEditForm.errors.price)
                  }
                  id="price"
                  name="price"
                  placeholder="Price"
                ></TextField>
              </div>
              <div className="editmodal__textfield">
                <p>Product Image:</p>
                <TextField
                  fullWidth
                  className={classes.textForm}
                  value={productEditForm.values.imageUrl}
                  onChange={productEditForm.handleChange}
                  error={
                    productEditForm.touched.imageUrl &&
                    Boolean(productEditForm.errors.imageUrl)
                  }
                  id="images"
                  name="images"
                  placeholder="Image"
                ></TextField>
              </div>
              <div className="editmodal__textfield">
                <p>Product Description:</p>
                <textarea
                  className="editmodal-textarea"
                  placeholder="Description"
                  value={productEditForm.values.description}
                  onChange={productEditForm.handleChange}
                  error={
                    productEditForm.touched.description &&
                    Boolean(productEditForm.errors.description)
                  }
                  id="description"
                  name="description"
                ></textarea>
              </div>
              <button className="submit-btn" type="submit">
                Apply Changes
              </button>
            </form>
          </div>
          <div
            className="editmodal__background"
            onClick={() => closeEditor()}
          ></div>
        </div>
      )
    );
  }
  return null;
};

export default EditModal;
