import SimpleImageSlider from "react-simple-image-slider";

const ImageSlider = () => {
  const images = [
    {
      url: "https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/4-scaled.jpg",
    },
    {
      url: " https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/filter1-scaled.jpg",
    },
    {
      url: "https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/2-scaled.jpg",
    },
  ];
  const sliderOptions = {
    useGPURender: true,
    showNavs: true,
    showBullets: false,
    navStyle: 1,
    navSize: 50,
    navMargin: 30,
    duration: 0.5,
    bgColor: "#000",
  };

  return (
    <SimpleImageSlider
      width={530}
      height={300}
      images={images}
      showBullets={sliderOptions.showBullets}
      showNavs={sliderOptions.showNavs}
      startIndex={0}
      useGPURender={sliderOptions.useGPURender}
      navStyle={sliderOptions.navStyle}
      navSize={sliderOptions.navSize}
      navMargin={sliderOptions.navMargin}
      slideDuration={sliderOptions.duration}
    />
  );
};

export default ImageSlider;
