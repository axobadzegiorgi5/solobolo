import "./landingpage.css";
import ReorderIcon from "@material-ui/icons/Reorder";
import sliderImage1 from "./landingcomponents/imageslider/sliderimage3.jpg";
import { useHistory } from "react-router-dom";
import LandingDrawer from "../common/drawer";
import { useDispatch } from "react-redux";
import { DrawerStateAction } from "../reducers/ProductAction";

const LandingPage = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const pushSignup = () => {
    if (localStorage.getItem("token")) {
      history.push("/catalog");
    } else {
      history.push("/register");
    }
  };

  const pushLogin = () => {
    if (localStorage.getItem("token")) {
      history.push("/catalog");
    } else {
      history.push("/login");
    }
  };

  return (
    <div className="landing">
      <header className="landing__header">
        <div className="landing__header-img">
          <LandingDrawer />
          <img
            alt="logo"
            src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/group-30.png"
          />
        </div>
        <ul className="landing__header-list">
          <li className="list-item">ABOUT</li>
          <li className="list-item">CATALOG</li>
          <li className="list-item">PRICING</li>
          <li className="list-item">SUPPLIERS</li>
          <li className="list-item">HELP CENTER</li>
          <li className="list-item">BLOG</li>
          <li className="list-item list-item-border" onClick={pushSignup}>
            SIGN UP NOW
          </li>
          <li className="list-item list-item-highlited" onClick={pushLogin}>
            LOGIN
          </li>
        </ul>
        <div
          className="landing__header-burger"
          onClick={() => dispatch(DrawerStateAction(true))}
        >
          <ReorderIcon></ReorderIcon>
        </div>
      </header>
      <div className="landing__slider slider-fullscreen">
        <div className="landing__slider-welcome">
          <img
            alt="logo"
            src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/356Logo.svg"
          ></img>
          <h4>WE GOT YOUR SUPPLY CHAIN COVERED</h4>
          <h4>365 DAYS A YEAR!</h4>
        </div>
        <button className="landing__base-btn " onClick={pushSignup}>
          SIGN UP
        </button>
      </div>
      <div className="landing__content">
        <div className="landing__heading">
          <h2>DISCOVER GLOBAL DROPSHIPPING PRODUCTS</h2>
          <p className="landing__heading-sub">
            PARTNER WITH THE MOST EXPERIENCED DROPSHIPPING TEAM IN THE INDUSTRY!
          </p>
          <div className="landing__heading-message">
            <span>
              <span className="text-highlighted">ONE</span> POINT OF CONTACT
              PROVIDING TOP DROPSHIP PRODUCTS FROM WORLDWIDE SUPPLIERS
            </span>
          </div>
          <div className="landing__heading-columns">
            <div className="column__container">
              <div className="column-image">
                <img
                  alt="columnimage"
                  src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/combined-shape-1.svg"
                />
              </div>
              <div className="column-text">
                <span className="column-text-top">70</span>
                <span className="column-text-bottom">Unique Suppliers</span>
              </div>
            </div>
            <div className="column__container">
              <div className="column-image">
                <img
                  alt="columnimage"
                  src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/combined-shape.svg"
                />
              </div>
              <div className="column-text">
                <span className="column-text-top">200,000</span>
                <span className="column-text-bottom">Products</span>
              </div>
            </div>
            <div className="column__container">
              <div className="column-image">
                <img
                  alt="columnimage"
                  src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/combined-shape-2.svg"
                />
              </div>
              <div className="column-text">
                <span className="column-text-top">150</span>
                <span className="column-text-bottom">Destinations</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="landing__explainer">
        <div className="landing__explainer-wrapper">
          <div className="landing__explainer-text">
            <h2 className="landing-header">HOW THE DROPSHIP BUSINESS WORKS?</h2>
            <span className="explainer-description">
              {" "}
              Browse and select from our catalog. More than thousands of
              Dropship products available on various niches. 365
              <span className="span-dark-blue">DROPSHIP</span> product catalog
              covers an extensive selection from some of the world’s best
              sellers
            </span>
          </div>
          <div className="landing__column-container">
            <div className="landing__column">
              <div className="landing__column-img">
                <img
                  src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/1212.png"
                  alt=""
                />
              </div>
              <div className="landing__column-bottom">
                <h3>JOIN US</h3>
                <span>
                  Register to 365
                  <span className="column-text-highlight">DROPSHIP</span>
                </span>
              </div>
            </div>
            <div className="landing__column">
              <div className="landing__column-img">
                <img
                  src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/a-2-1.png"
                  alt=""
                />
              </div>
              <div className="landing__column-bottom">
                <h3>FILL YOUR STORE INVENTORY</h3>
                <span>
                  Choose, manage and sync products from the catalog directly to
                  your online store
                </span>
              </div>
            </div>
            <div className="landing__column">
              <div className="landing__column-img">
                <img
                  src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/a-3-1.png"
                  alt=""
                />
              </div>
              <div className="landing__column-bottom">
                <h3>GET ORDERS</h3>
                <span>
                  Choose manual or automatic synchronize between your online
                  store orders and your shopping cart
                </span>
              </div>
            </div>
            <div className="landing__column">
              <div className="landing__column-img">
                <img
                  src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/a-4-1.png"
                  alt=""
                />
              </div>
              <div className="landing__column-bottom">
                <h3>DIRECT SHIPPING</h3>
                <span>
                  We will automatically ship your orders directly to your
                  customer
                </span>
              </div>
            </div>
          </div>
          <button className="landing__base-btn get-started-btn">
            GET STARTED
          </button>
        </div>
      </div>
      <div className="landing__benefits">
        <h2 className="landing-header">BENEFITS</h2>
        <span className="benefits-description">
          Easily sync dropship products into your online shop.
        </span>
        <span className="benefits-description">
          Manage your process manually or automatically, exporting all product
          details
        </span>
        <div className="benefits__columns">
          <div className="benefits__columns-left">
            <div className="benefits__column">
              <div className="benefits__column-img">
                <img
                  src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/group-52.svg"
                  alt=""
                />
              </div>
              <div className="benefits__column-text">
                <h3 className="benefits__column-title">
                  Only one point of contact
                </h3>
                <span className="benefits__column-description">
                  365DROPSHIP will take care of the supply chain, no need to
                  talk to hundreds of different suppliers, just choose the
                  products and we will take care of the rest!
                </span>
              </div>
            </div>
            <div className="benefits__column">
              <div className="benefits__column-img">
                <img
                  src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/group-52.svg"
                  alt=""
                />
              </div>
              <div className="benefits__column-text">
                <h3 className="benefits__column-title">Profit %</h3>
                <span className="benefits__column-description">
                  <span>
                    365DROPSHIP understands the business of dropshipping to its
                    core and knows the value of a good deal.
                  </span>
                  <span>
                    In 365DROPSHIP catalog you will find great products with the
                    best profit margins available!
                  </span>
                </span>
              </div>
            </div>
          </div>
          <div className="benefits__columns-right">
            <div className="benefits__column">
              <div className="benefits__column-img">
                <img
                  src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/group-52.svg"
                  alt=""
                />
              </div>
              <div className="benefits__column-text">
                <h3 className="benefits__column-title">
                  Unique international suppliers
                </h3>
                <span className="benefits__column-description">
                  365DROPSHIP experienced team works only with trusted and
                  secured suppliers chosen from all over the world! Every
                  Supplier at 365DROPSHIP is certified
                </span>
              </div>
            </div>
            <div className="benefits__column">
              <div className="benefits__column-img">
                <img
                  src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/group-52.svg"
                  alt=""
                />
              </div>
              <div className="benefits__column-text">
                <h3 className="benefits__column-title">Most advanced UI</h3>
                <span className="benefits__column-description">
                  The 365DROPSHIP platform has been around for a while, but we
                  recently updated all of our technology in order to supply the
                  best user experience for you and for your customers. Come and
                  check it out!
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="landing__video">
        <h2 className="landing-header">DROPSHIP</h2>
        <iframe
          alt=""
          title="How 365Dropship Works"
          src="https://www.youtube.com/embed/ig3CLaRdjk4?feature=oembed"
          frameborder="0"
          allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowfullScreen=""
        ></iframe>
      </div>
      <div className="landing__features">
        <h2 className="landing-header features-header">
          DROPSHIP PRODUCT FEATURES
        </h2>
        <ul className="features__list">
          <li className="features__list-item features__list-item-highlight">
            <img
              src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/1-1.svg"
              alt=""
            ></img>
            <span>Advanced UI</span>
          </li>
          <li className="features__list-item">
            <img
              src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/6-1.svg"
              alt=""
            ></img>
            <span>Activity Dashboard</span>
          </li>
          <li className="features__list-item">
            <img
              src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/2-1.svg"
              alt=""
            ></img>
            <span>Products sync</span>
          </li>
          <li className="features__list-item">
            <img
              src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/3-1.svg"
              alt=""
            ></img>
            <span>Transaction Board</span>
          </li>
          <li className="features__list-item">
            <img
              src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/4-1.svg"
              alt=""
            ></img>
            <span>Supply & Support</span>
          </li>
          <li className="features__list-item">
            <img
              src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/5-1.svg"
              alt=""
            ></img>
            <span>Account Setup</span>
          </li>
        </ul>
        <div className="features__information">
          <div className="features__information-wrapper">
            <img className="features__slider" src={sliderImage1} alt=""></img>
            <div className="features__information-text">
              <h3>Advanced UI</h3>
              <p>
                In the 365DROPSHIP platform you’ll find the most user-friendly
                UI enabling easy product search, inventory management, and order
                placement.
              </p>

              <p>
                Our User Interface is simple and sophisticated at the same time.
                Using a modern product search system, you can find anything you
                need in seconds.
              </p>
              <p>
                Here you can easily manage your inventory and place the desired
                orders. The interface is adjusted to your needs and designed so
                that everything is straightforward from your first click. Our
                advanced user interface saves your time and allows you to easily
                surf through the dropship platform features.
              </p>
            </div>
          </div>
          <button className="landing__base-btn features-btn">
            GET STARTED
          </button>
        </div>
      </div>
      <div className="landing__integrations">
        <h2 className="landing-header integrations-header">INTEGRATIONS</h2>
        <ul className="landing__integrations-list">
          <li>
            <img
              src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/wix-com-website-logo.svg"
              alt=""
            />
          </li>
          <li>
            <img
              src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/shopify-logo-2018.svg"
              alt=""
            />
          </li>
          <li>
            <img
              src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/magento.svg"
              alt=""
            />
          </li>
          <li>
            <img
              src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/woo-commerce-logo-woo-commerce.png"
              alt=""
            />
          </li>
        </ul>
      </div>
      <div className="landing__footer">
        <ul className="footer__list">
          <li className="footer__list-item footer__list-item-active">
            WHAT IS DROPSHIPPING
          </li>
          <li className="footer__list-item">FEATURES</li>
          <li className="footer__list-item">ABOUT</li>
          <li className="footer__list-item">WHY 365DROPSHIP</li>
        </ul>
        <div className="landing__footer-text">
          <strong>What is 365DROPSHIP? </strong>
          <p>
            <span>
              365dropship allows you to add high demand dropship products from
              our extensive catalog to sell at your store, ship the orders
              directly to customers all over the world, and help increase your
              profits an excellent solution for people with existing stores
              anywhere online and those of you looking to break into the
              eCommerce market. We propose you to download dropshipping products
              from the hundreds of best-selling products from a variety of
              niches with affordable prices to choose from our catalog. We have
              teamed up with some of the best worldwide drop ship suppliers in
              order to create an attractive line of products, thousands of{" "}
            </span>
          </p>
          <strong>What is dropshipping?</strong>
          <p>
            <span>
              Selling high demand products without actually stocking or
              shipping. Never worrying about an item not selling. You only pay
              for what you sell. Access to the hottest name brand products from
              our huge inventory catalog to sell on your store. Risk-free online
              business creation and management. Focusing on the important stuff
              that you do best: Generating traffic and sales.<br></br>
              Our dropshipping service is ideal for anyone, no matter what
              dropship items your customers may choose they’ll be getting
              quality and name-brand pieces. You can choose products from basic,
              everyday apparel to luxurious selections. 365 Dropship platform
              makes it easy to automate dropshipping products to your eCommerce
              store.<br></br>
              <br></br>
              Tap into our product catalog and choose from a large variety of
              products to build your business inventory. With our highly
              advanced Dropship integration tool, you can sync our dropshipping
              product data feed and eCommerce tools directly with your current
              website.<br></br>
              <br></br>
              You can sell our products on the most popular platforms. With our
              easy to use automatic product feed tools. if you wanted to sell
              our products on Shopify, WooCommerce or Magento, then you would
              simply choose the items you want to sell and export them to your
              site. All our feeds are per-formatted to meet the requirements of
              several marketplace websites so you don’t have to worry about
              maintaining the feeds yourself.
            </span>
          </p>
        </div>
      </div>
      <footer className="landing__mainfooter">
        <ul className="mainfooter__list">
          <li className="mainfooter__list-item mainfooter__list-item-gray">
            <img
              src="https://mk0q365dropshipe482k.kinstacdn.com/wp-content/uploads/2020/06/logo.png"
              alt=""
            />
            <p>
              {" "}
              365DropShip is a wholesale drop shipping business that provides
              products and order fulfillment.
            </p>
          </li>
          <li className="mainfooter__list-item">
            <h3>DROPSHIP PRODUCTS</h3>
            <ul>
              <li>Home & Design</li>
              <li>Fashion</li>
              <li>Jewlery</li>
              <li>Beauty</li>
            </ul>
          </li>
          <li className="mainfooter__list-item">
            <h3>ABOUT</h3>
            <ul>
              <li>DROPSHIP SUPPLIERS</li>
              <li>Company</li>
              <li>Pricing</li>
              <li>Contact Us</li>
            </ul>
          </li>
          <li className="mainfooter__list-item">
            <h3>DROPSHIP PRODUCTS</h3>
            <ul>
              <li>Help Center</li>
              <li>Terms of Service</li>
              <li>Privacy Policy</li>
            </ul>
          </li>
        </ul>
      </footer>
      <div className="mainfooter-bottom">
        <span>© copyright 365Dropship. All Rights Reserved.</span>
      </div>
    </div>
  );
};

export default LandingPage;
