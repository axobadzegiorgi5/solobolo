import "./navigation.css";
import { ReactComponent as CartSVG } from "./navIcons/Cart.svg";
import { ReactComponent as CatalogSVG } from "./navIcons/Catalog.svg";
import { ReactComponent as DashboardSVG } from "./navIcons/Dashboard.svg";
import { ReactComponent as InventorySVG } from "./navIcons/Inventory.svg";
import { ReactComponent as OrdersSVG } from "./navIcons/Orders.svg";
import profilePNG from "./navIcons/profile.png";
import { ReactComponent as StoreslistSVG } from "./navIcons/StoresList.svg";
import { ReactComponent as TransactionsSVG } from "./navIcons/Transactions.svg";
import { useHistory, NavLink } from "react-router-dom";
const Navigation = () => {
  const history = useHistory();

  return (
    <div className="navigation">
      <div className="navigation__logo">
        <img
          src="https://app.365dropship.com/assets/images/dropship_logo.png"
          alt="logo"
        ></img>
      </div>
      <div className="navigation__content">
        <img
          className="navigation-profile"
          src={profilePNG}
          alt="profile"
          onClick={() => history.push("/profile")}
        />
        <ul className="navigation__list">
          <NavLink to="/dashboard">
            <li className="navigation__list-item">
              <DashboardSVG />
            </li>
          </NavLink>
          <NavLink to="/catalog">
            <li
              className="navigation__list-item"
              onClick={() => history.push("/catalog")}
            >
              <CatalogSVG />
            </li>
          </NavLink>

          <NavLink to="/inventory">
            <li className="navigation__list-item">
              <InventorySVG />
            </li>
          </NavLink>
          <NavLink to="/cart">
            <li className="navigation__list-item">
              <CartSVG />
            </li>
          </NavLink>
          <NavLink to="/orders">
            <li className="navigation__list-item">
              <OrdersSVG />
            </li>
          </NavLink>
          <NavLink to="/transactions">
            <li className="navigation__list-item">
              <TransactionsSVG />
            </li>
          </NavLink>
          <NavLink to="/storeslist">
            <li className="navigation__list-item">
              <StoreslistSVG />
            </li>
          </NavLink>
        </ul>
      </div>
    </div>
  );
};

export default Navigation;
