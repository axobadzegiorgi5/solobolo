import axios from "axios";

export const BASE_URL = "http://18.185.148.165:3000";
axios.interceptors.request.use((config) => {
  config.headers.Authorization = `Bearer ${localStorage.getItem("token")}`;
  return config;
});

export const Register = async (email, password) => {
  const result = await axios.post(BASE_URL + "/register", {
    password: password,
    firstName: "John",
    lastName: "Doe",
    email: email,
    passwordConfirmation: password,
  });
  return result;
};

export const Login = async (email, password) => {
  const result = await axios.post(BASE_URL + "/login", {
    email: email,
    password: password,
  });
  return result;
};

export const getProducts = async () => {
  const result = await axios.get(BASE_URL + "/api/v1/products");
  return result;
};

export const deleteProductAPI = async (id) => {
  const result = await axios.delete(BASE_URL + `/api/v1/products/${id}`);
  return result;
};

export const addToCartAPI = async (productId, qty) => {
  const result = await axios.post(BASE_URL + "/api/v1/cart/add", {
    productId,
    qty,
  });

  return result;
};

export const addProductAPI = async (title, description, price, imageUrl) => {
  const result = await axios.post(BASE_URL + "/api/v1/products", {
    title,
    description,
    price,
    imageUrl,
  });
  return result;
};

export const cartFetchAPI = async () => {
  const result = await axios.get(BASE_URL + "/api/v1/cart");
  return result;
};

export const deleteCartItemAPI = async (id) => {
  const result = await axios.post(BASE_URL + `/api/v1/cart/remove/${id}`);
  return result;
};

export const updateCartAPI = async (id) => {
  const result = await axios.get(BASE_URL + `/api/v1/products/${id}`);
  return result;
};

export const singleProductAPI = async (id) => {
  const result = await axios.get(BASE_URL + `/api/v1/products/${id}`);
  return result;
};

export const editProductAPI = async (
  id,
  title,
  description,
  price,
  imageUrl
) => {
  const result = await axios.put(BASE_URL + `/api/v1/products/${id}`, {
    title,
    description,
    price,
    imageUrl,
  });
  return result;
};

export const editProfileAPI = async (
  id,
  firstName,
  lastName,
  email,
  password
) => {
  const result = await axios.put(BASE_URL + `/api/v1/users/${id}`, {
    firstName,
    lastName,
    email,
    password,
  });
  return result;
};
