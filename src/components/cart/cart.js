import CustomTable from "./table";
import "./cart.css";
import { useSelector } from "react-redux";
import MenuIcon from "@material-ui/icons/Menu";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import { useHistory } from "react-router";
const Cart = () => {
  const cartProductsList = useSelector((state) => state.cartProducts);

  const history = useHistory();

  return (
    <div className="cart">
      <div className="cart__header">
        <span className="cart__header-title">
          SHOPPING CART ({cartProductsList.length})
        </span>
        <div className="cart__header-right">
          <div className="cart__header-burger">
            <MenuIcon />
          </div>
          <button className="help-menu">
            <span>?</span>
          </button>
        </div>
      </div>
      <div className="table-wrapper">
        <div className="cart__table-container">
          <CustomTable />
        </div>
      </div>
      <div className="cart__bottom--section">
        <div className="cart__shipping">
          <div className="cart__shipping--title">Shipping details</div>
          <div className="cart__shipping-rows">
            <div className="shipping-row">
              <span className="shipping-row-title">
                First Name <span className="required">*</span>
              </span>
              <input type="text" className="shipping-row-input"></input>

              <span className="shipping-row-title">
                Country <span className="required">*</span>
              </span>
              <input type="text" className="shipping-row-input"></input>
            </div>
            <div className="shipping-row">
              <span className="shipping-row-title">
                Last Name <span className="required">*</span>
              </span>
              <input type="text" className="shipping-row-input"></input>

              <span className="shipping-row-title">
                City <span className="required">*</span>
              </span>
              <input type="text" className="shipping-row-input"></input>
            </div>
            <div className="shipping-row">
              <span className="shipping-row-title">
                Email <span className="required">*</span>
              </span>
              <input type="text" className="shipping-row-input"></input>

              <span className="shipping-row-title">
                Street Address <span className="required">*</span>
              </span>
              <input type="text" className="shipping-row-input"></input>
            </div>
            <div className="shipping-row">
              <span className="shipping-row-title">Phone</span>
              <input type="text" className="shipping-row-input"></input>

              <span className="shipping-row-title">
                Zip <span className="required">*</span>
              </span>
              <input type="text" className="shipping-row-input"></input>
            </div>
            <div className="shipping-row shipping-row--margin">
              <span className="shipping-row-title">Message</span>
              <input
                type="text"
                className="shipping-row-input shipping-row-input--long"
              ></input>
            </div>
          </div>
        </div>
        <div className="cart__checkout">
          <div className=" cart__checkout--title">shipping method</div>
        </div>
      </div>
      <div className="cart__info">
        <div className="cart__info-left">
          <button
            className="continue-shopping-btn"
            onClick={() => history.push("/catalog")}
          >
            <ArrowBackIcon />
            Continue Shopping
          </button>
        </div>
        <div className="cart__info-right">
          <div className="cart__info-prices">
            <div className="total-section">
              <span className="total-label">BALANCE:</span>
              <span className="total-number">$0.00</span>
            </div>
            <div className="total-section">
              <span className="total-label">ITEMS TOTAL:</span>
              <span className="total-number">$0.00</span>
            </div>
            <div className="total-section">
              <span className="total-label">SHIPPING TOTAL:</span>
              <span className="total-number">$0.00</span>
            </div>
            <div className="total-section">
              <span className="total-label">ORDER TOTAL:</span>
              <span className="total-number"> $0.00</span>
            </div>
          </div>
          <button className="checkout-btn">Checkout</button>
        </div>
      </div>
    </div>
  );
};

export default Cart;
