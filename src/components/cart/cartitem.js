import { useDispatch, useSelector } from "react-redux";
import { addToCartAPI, cartFetchAPI, deleteCartItemAPI } from "../API";
import { getCartAction, snackbarStateAction } from "../reducers/ProductAction";
import DeleteIcon from "@material-ui/icons/Delete";
import { useEffect } from "react";
const CartItem = ({ id, title, image, qty, price, key }) => {
  const dispatch = useDispatch();

  const deleteCartItem = async (Id) => {
    try {
      await deleteCartItemAPI(Id);
      await cartFetchAPI().then((res) =>
        dispatch(getCartAction(res.data.data.cartItem.items))
      );
      await dispatch(
        snackbarStateAction({
          state: true,
          message: "Product Deleted From Cart Successfully",
          severity: "success",
        })
      );
    } catch (err) {
      await dispatch(
        snackbarStateAction({
          state: true,
          message: `${err}`,
          severity: "error",
        })
      );
    }
  };
  const cartProductsList = useSelector((state) => state.cartProducts);

  const checkQty = async () => {
    try {
      await deleteCartItemAPI(id);
      await cartFetchAPI().then((res) =>
        dispatch(getCartAction(res.data.data.cartItem.items))
      );
      await dispatch(
        snackbarStateAction({
          state: true,
          message: "Product Deleted From Cart Successfuly",
          severity: "success",
        })
      );
    } catch (err) {
      await deleteCartItemAPI(id);
      await cartFetchAPI().then((res) =>
        dispatch(getCartAction(res.data.data.cartItem.items))
      );
      await dispatch(
        snackbarStateAction({
          state: true,
          message: `${err}`,
          severity: "error",
        })
      );
    }
  };

  useEffect(() => {
    if (qty < 1) {
      checkQty();
    }
  }, [cartProductsList]);

  const addQuantity = async (Id, Qty) => {
    try {
      await addToCartAPI(Id, Qty);
      await cartFetchAPI().then((res) =>
        dispatch(getCartAction(res.data.data.cartItem.items))
      );
    } catch (err) {
      await dispatch(
        snackbarStateAction({
          state: true,
          message: `${err}`,
          severity: "error",
        })
      );
    }
  };

  return (
    <tr className="table-item" key={id}>
      <td className="table-item-description">
        <div className="table-img">
          <img alt="product" src={image} />
        </div>
        <span>{title}</span>
      </td>
      <td className="table-item table-item-supplier">SP-Supplier115</td>
      <td className="table-item table-item-options">1</td>
      <td className="table-item table-item-quantity">
        <div className="qty-input">
          <div
            className="qty-minus qty-btn"
            onClick={() => addQuantity(id, -1)}
          >
            -
          </div>
          {qty}
          <div className="qty-plus qty-btn" onClick={() => addQuantity(id, 1)}>
            +
          </div>
        </div>
      </td>
      <td className="table-item table-item-itemcost">${price}</td>
      <td className="table-item table-item-vat">$7.68</td>
      <td className="table-item table-item-totalcost">${price}</td>
      <td className="table-item table-item-delete">
        <div className="delete-svg" onClick={() => deleteCartItem(id)}>
          <DeleteIcon />
        </div>
      </td>
    </tr>
  );
};

export default CartItem;
