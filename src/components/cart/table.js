import "./table.css";

import { cartFetchAPI } from "../API";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getCartAction } from "../reducers/ProductAction";
import CartItem from "./cartitem";

const CustomTable = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    cartFetchAPI().then((res) =>
      dispatch(getCartAction(res.data.data.cartItem.items))
    );
  }, []);

  const cartProductsList = useSelector((state) => state.cartProducts);

  return (
    <table className="table" cellSpacing="0" cellPadding="0">
      <tr className="table__header">
        <th className="table-description">ITEM DESCRIPTION</th>
        <th>SUPPLIER</th>
        <th>OPTIONS</th>
        <th>QUANTITY</th>
        <th>ITEM COST</th>
        <th>VAT</th>
        <th>TOTAL COST</th>
        <th></th>
      </tr>
      {cartProductsList.map((product) => (
        <CartItem
          id={product.id}
          title={product.title}
          image={product.image}
          qty={product.qty}
          price={product.price}
        />
      ))}
    </table>
  );
};

export default CustomTable;
