import "./profile.css";
import ProfileForm from "./profileform";
import { useHistory } from "react-router";
import { useEffect } from "react";
import MenuIcon from "@material-ui/icons/Menu";
import { useDispatch } from "react-redux";
import { DrawerStateAction } from "../reducers/ProductAction";
import CatalogDrawer from "../common/catalogDrawer";
const Profile = () => {
  const history = useHistory();
  const dispatch = useDispatch();

  const signOut = async () => {
    await localStorage.removeItem("user");
    await localStorage.removeItem("token");
    await history.push("/");
  };

  useEffect(() => {
    if (localStorage.getItem("token") === null) {
      history.push("/");
    }
  }, [history]);

  return (
    <div className="profile__wrapper">
      <CatalogDrawer />
      <header className="profile__header">
        <span className="my-profile">MY PROFILE</span>
        <div className="header-btn-wrapper">
          <button className="sign-out" onClick={() => signOut()}>
            SIGN OUT
          </button>
          <div
            className="header__burger"
            onClick={() => dispatch(DrawerStateAction(true))}
          >
            <MenuIcon />
          </div>
          <button className="help-menu">?</button>
        </div>
      </header>

      <div className="profile__main">
        <ProfileForm />
      </div>
    </div>
  );
};

export default Profile;
