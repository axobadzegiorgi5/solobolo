import { TextField } from "@material-ui/core";
import { useFormik } from "formik";
import { useDispatch } from "react-redux";
import * as Yup from "yup";
import { editProfileAPI } from "../API";
import { snackbarStateAction } from "../reducers/ProductAction";
const ProfileEditValidation = Yup.object({
  firstName: Yup.string().min(5, "Must be at least 5 characters"),
  lastName: Yup.string().min(5, "Must be at least 5 characters"),
  email: Yup.string().email(),
  password: Yup.string().min(5, "Must be at least 5 characters"),
});

const ProfileForm = () => {
  const User = JSON.parse(localStorage.getItem("user"));
  const dispatch = useDispatch();
  const profileEditForm = useFormik({
    initialValues: {
      firstName: User && User.firstName,
      lastName: User && User.lastName,
      email: User && User.email,
      password: "",
    },

    onSubmit: async (values) => {
      try {
        await editProfileAPI(
          User.id,
          values.firstName,
          values.lastName,
          values.email,
          values.password
        );
        await dispatch(
          snackbarStateAction({
            state: "true",
            message: "Profile Changes Saved Successfully",
            severity: "success",
          })
        );
      } catch (err) {
        await dispatch(
          snackbarStateAction({
            state: "true",
            message: `${err}`,
            severity: "error",
          })
        );
      }
    },
    validationSchema: ProfileEditValidation,
    enableReinitialize: true,
  });

  return (
    <form className="profile__avatar" onSubmit={profileEditForm.handleSubmit}>
      <div className="avatar-heading">
        <div className="avatar-widgets">
          <ul className="avatar-ul">
            <li className="avatar-li avatar-li--active">PROFILE</li>
            <li className="avatar-li">BILLING</li>
            <li className="avatar-li avatar-li--wide">INVOICE HISTORY</li>
          </ul>
        </div>
        <div className="avatar-btns">
          <button className="deactivate-account" type="button">
            DEACTIVATE ACCOUNT
          </button>
        </div>
      </div>
      <div className="avatar-wrapper">
        <div className="avatar-wrapper-left">
          <div className="avatar-content-wrapper">
            <span className="avatar-content-header">profile picture</span>
            <div className="avatar-content">
              <div className="avatar-image">
                <img
                  src="https://app.365dropship.com/assets/images/profile-example.jpg"
                  alt="profile image"
                ></img>
              </div>
              <button className="upload-image">Upload</button>
            </div>
          </div>
          <div className="avatar-content-wrapper">
            <span className="avatar-content-header"> Change Password</span>
            <div className="avatar-content">
              <div className="avatar-input">
                <label>Current Password</label>
                <TextField placeholder="Current Password"></TextField>
              </div>
              <div className="avatar-input">
                <label>New Password</label>
                <TextField
                  placeholder="New Password"
                  value={profileEditForm.values.password}
                  onChange={profileEditForm.handleChange}
                  error={
                    profileEditForm.touched.password &&
                    Boolean(profileEditForm.errors.password)
                  }
                  helperText={
                    profileEditForm.touched.password &&
                    profileEditForm.errors.password
                  }
                  id="password"
                  name="password"
                ></TextField>
              </div>
              <div className="avatar-input">
                <label>Confirm New Password</label>
                <TextField placeholder="Confirm New Password"></TextField>
              </div>
            </div>
          </div>
        </div>

        <div className="avatar-wrapper-right">
          <div className="avatar-content-wrapper">
            <span className="avatar-content-header">Personal Details</span>
            <div className="avatar-content">
              <div className="avatar-input">
                <label>First Name</label>
                <TextField
                  placeholder="First Name"
                  value={profileEditForm.values.firstName}
                  onChange={profileEditForm.handleChange}
                  error={
                    profileEditForm.touched.firstName &&
                    Boolean(profileEditForm.errors.firstName)
                  }
                  helperText={
                    profileEditForm.touched.firstName &&
                    profileEditForm.errors.firstName
                  }
                  id="firstName"
                  name="firstName"
                />
              </div>
              <div className="avatar-input">
                <label>Last Name</label>
                <TextField
                  placeholder="Last Name"
                  value={profileEditForm.values.lastName}
                  onChange={profileEditForm.handleChange}
                  error={
                    profileEditForm.touched.lastName &&
                    Boolean(profileEditForm.errors.lastName)
                  }
                  helperText={
                    profileEditForm.touched.lastName &&
                    profileEditForm.errors.lastName
                  }
                  id="lastName"
                  name="lastName"
                ></TextField>
              </div>
              <div className="avatar-input">
                <label>Country</label>
                <TextField placeholder="Country"></TextField>
              </div>
            </div>
          </div>
          <div className="avatar-content-wrapper">
            <span className="avatar-content-header">contact information</span>
            <div className="avatar-content">
              <div className="avatar-input">
                <label>Email</label>
                <TextField
                  placeholder="Email"
                  value={profileEditForm.values.email}
                  onChange={profileEditForm.handleChange}
                  error={
                    profileEditForm.touched.email &&
                    Boolean(profileEditForm.errors.email)
                  }
                  helperText={
                    profileEditForm.touched.email &&
                    profileEditForm.errors.email
                  }
                  id="email"
                  name="email"
                ></TextField>
              </div>
              <div className="avatar-input">
                <label>Skype</label>
                <TextField placeholder="Skype"></TextField>
              </div>
              <div className="avatar-input">
                <label>Phone</label>
                <TextField placeholder="Phone"></TextField>
              </div>
            </div>
          </div>
        </div>
      </div>
      <button className="save-profile-btn" type="submit">
        Save Changes
      </button>
    </form>
  );
};

export default ProfileForm;
