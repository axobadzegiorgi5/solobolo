import React from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import TextField from "@material-ui/core/TextField";
import { useHistory } from "react-router-dom";
import { Login } from "../API";
import "./login.css";
import { makeStyles } from "@material-ui/core/styles";
import { useDispatch } from "react-redux";
import { DrawerStateAction } from "../reducers/ProductAction";

const loginValidation = Yup.object({
  email: Yup.string().required("Email Required").email("Invalid Email"),
  password: Yup.string().required("Password Required"),
});

const LoginForm = () => {
  const useStyles = makeStyles({
    field: {
      borderRadius: 4,
      marginBottom: "30px",
    },
    logbutton: {
      background: "#61d5df",
      border: 0,
      borderRadius: 4,
      color: "white",
      padding: "15px 30px",
      width: "333px",
      marginTop: "0px",
      fontFamily: "sans-serif",
      fontWeight: "500",
      "&:hover": {
        backgroundColor: "#61d5df",
      },
    },
    button: {
      background: "#61d5df",
      border: 0,
      borderRadius: 4,
      color: "white",
      padding: "15px 30px",
      width: "333px",
      marginTop: "20px",
      fontFamily: "sans-serif",
      fontWeight: "500",
      "&:hover": {
        backgroundColor: "#61d5df",
      },
    },
  });
  const classes = useStyles();

  const history = useHistory();
  const dispatch = useDispatch();

  const loginForm = useFormik({
    initialValues: { email: "", password: "" },
    onSubmit: (values) => {
      Login(values.email, values.password).then((res) => {
        if ((res.data.data.success = true)) {
          localStorage.setItem("user", JSON.stringify(res.data.data));
          localStorage.setItem(
            "token",
            JSON.stringify(res.data.data.token).replace(/['"]+/g, "")
          );
          dispatch(DrawerStateAction(false));
          history.push("/catalog");
        }
      });
    },
    validationSchema: loginValidation,
  });

  return (
    <form onSubmit={loginForm.handleSubmit}>
      <TextField
        className={classes.field}
        error={loginForm.touched.email && Boolean(loginForm.errors.email)}
        value={loginForm.values.email}
        onChange={loginForm.handleChange}
        helperText={loginForm.touched.email && loginForm.errors.email}
        name="email"
        id="email"
        variant="outlined"
        placeholder="E-mail"
      />
      <TextField
        className={classes.field}
        error={loginForm.touched.password && Boolean(loginForm.errors.password)}
        value={loginForm.values.password}
        onChange={loginForm.handleChange}
        helperText={loginForm.touched.password && loginForm.errors.password}
        name="password"
        id="password"
        variant="outlined"
        placeholder="Password"
        type="password"
      />
      <div className="signup__terms">
        <span>Forgot Password?</span>
      </div>
      <button type="submit" className="signup-btn">
        Log In
      </button>
    </form>
  );
};

export default LoginForm;
