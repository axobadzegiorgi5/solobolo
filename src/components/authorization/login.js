import LoginForm from "./loginform";
import { Link, useHistory } from "react-router-dom";
import "./login.css";
import { useEffect } from "react";
const Login = () => {
  const history = useHistory();
  useEffect(() => {
    if (localStorage.getItem("token")) {
      history.push("/catalog");
    }
  }, [history]);

  return (
    <div className="login">
      <div className="login__form">
        <div className="login__logo">
          <img
            src="https://app.365dropship.com/assets/images/auth/logo.svg"
            alt="logo"
          />
        </div>
        <div className="login__heading">
          <h3>Members Log In</h3>
        </div>
        <LoginForm />
        <div className="login__social-auths">
          <div className="social__title">
            <span>Or Sign In With</span>
            <div></div>
          </div>
          <div className="social__buttons">
            <button className="social__button social__button-gmail"></button>
            <button className="social__button social__button-facebook"></button>
          </div>
          <div className="social__get-started">
            <span>Don't have an account?</span>{" "}
            <Link to="/register">Sign up</Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
