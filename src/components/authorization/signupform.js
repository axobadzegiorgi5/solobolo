import React from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import TextField from "@material-ui/core/TextField";
import { useHistory } from "react-router-dom";
import { Register } from "../API";
import "./signup.css";
import { makeStyles } from "@material-ui/core/styles";
import { useDispatch } from "react-redux";
import { DrawerStateAction } from "../reducers/ProductAction";

const signupValidation = Yup.object({
  email: Yup.string().required("Email Required").email("Invalid Email"),
  password: Yup.string().required("Password Required"),
});

const SignupForm = () => {
  const useStyles = makeStyles({
    field: {
      borderRadius: 4,
      marginBottom: "30px",
    },
    logbutton: {
      background: "#61d5df",
      border: 0,
      borderRadius: 4,
      color: "white",
      padding: "15px 30px",
      width: "333px",
      marginTop: "0px",
      fontFamily: "sans-serif",
      fontWeight: "500",
      "&:hover": {
        backgroundColor: "#61d5df",
      },
    },
    button: {
      background: "#61d5df",
      border: 0,
      borderRadius: 4,
      color: "white",
      padding: "15px 30px",
      width: "333px",
      marginTop: "20px",
      fontFamily: "sans-serif",
      fontWeight: "500",
      "&:hover": {
        backgroundColor: "#61d5df",
      },
    },
  });
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();

  const registerForm = useFormik({
    initialValues: { email: "", password: "" },
    onSubmit: (values) => {
      try {
        Register(values.email, values.password).then((res) => {
          if ((res.data.data.success = true)) {
            history.push("/catalog");
            localStorage.setItem("user", JSON.stringify(res.data.data));
            localStorage.setItem(
              "token",
              JSON.stringify(res.data.data.token).replace(/['"]+/g, "")
            );
            dispatch(DrawerStateAction(false));
          }
          console.log(res);
        });
      } catch (err) {
        alert(err);
      }
    },
    validationSchema: signupValidation,
  });

  return (
    <form onSubmit={registerForm.handleSubmit}>
      <TextField
        className={classes.field}
        error={registerForm.touched.email && Boolean(registerForm.errors.email)}
        value={registerForm.values.email}
        onChange={registerForm.handleChange}
        // helperText={registerForm.touched.email && registerForm.errors.email}
        name="email"
        id="email"
        variant="outlined"
        placeholder="E-mail"
      />
      <TextField
        className={classes.field}
        error={
          registerForm.touched.password && Boolean(registerForm.errors.password)
        }
        value={registerForm.values.password}
        onChange={registerForm.handleChange}
        // helperText={registerForm.touched.password && registerForm.errors.password}
        name="password"
        id="password"
        variant="outlined"
        placeholder="Password"
        type="password"
      />
      <div className="signup__terms">
        <span>
          By creating an account, you agree with the<br></br>
        </span>
        <span>Terms & Conditions and Privacy Policy</span>
      </div>
      <button type="submit" className="signup-btn">
        Sign Up
      </button>
    </form>
  );
};

export default SignupForm;
