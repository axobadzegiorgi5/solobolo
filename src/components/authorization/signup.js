import React, { useEffect } from "react";
import { Link, useHistory } from "react-router-dom";
import "./signup.css";
import SignupForm from "./signupform";

const Signup = () => {
  const history = useHistory();
  useEffect(() => {
    if (localStorage.getItem("token")) {
      history.push("/catalog");
    }
  }, [history]);
  return (
    <div className="signup">
      <div className="signup__form">
        <div className="signup__logo">
          <img
            src="https://app.365dropship.com/assets/images/auth/logo.svg"
            alt="logo"
          />
        </div>
        <div className="signup__heading">
          <h3>Sign Up</h3>
        </div>
        <SignupForm />
        <div className="signup__social-auths">
          <div className="social__title">
            <span>Or Sign In With</span>
            <div></div>
          </div>
          <div className="social__buttons">
            <button className="social__button social__button-gmail"></button>
            <button className="social__button social__button-facebook"></button>
          </div>
          <div className="social__get-started">
            <span>Already have an account?</span>{" "}
            <Link to="/login">Sign in</Link>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Signup;
